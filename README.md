Solo game jam inspired by the theme "dodging" provided by ScoreSpace Jam #13 (https://itch.io/jam/scorejam13).

Arcade-style spaceship game where the purpose is to dodge incoming asteroids and accumulate as many points as possible