﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spaceship : MonoBehaviour
{
    
    [SerializeField]
    private float _speed = 5.0f;
    [SerializeField]
    private int _score;
    [SerializeField]
    private int _lives = 3;
    private int _livesUi = 3;

    private UIManager _uiManager;
    private SpawnManager _spawnManager;


    void Start()
    {
        //Assign starting position
        transform.position = new Vector3(-0.63f, -22.5f, 0);

        _uiManager = GameObject.Find("Canvas").GetComponent<UIManager>();
        _spawnManager = GameObject.Find("Spawn_Manager").GetComponent<SpawnManager>();

        if (_uiManager == null)
        {
            Debug.Log("The UI Manager is NULL.");
        }

    }


    void Update()
    {
        CalculateMovement();
    }

    void CalculateMovement()
    {
        float horizontalInput = Input.GetAxisRaw("Horizontal");
        float verticalInput = Input.GetAxisRaw("Vertical");

        Vector3 direction = new Vector3(horizontalInput, verticalInput, 0);

        transform.Translate(direction * _speed * Time.deltaTime);


        //Sets the player bounds on the y-axis
        if (transform.position.y <= -22.9f)
        {
            transform.position = new Vector3(transform.position.x, -22.9f, 0);
        }
        else if (transform.position.y >= -18.4f)
        {
            transform.position = new Vector3(transform.position.x, -18.4f, 0);
        }
        
        //Sets the player bounds on the x-axis
        if  (transform.position.x <= -5.6f)
        {
            transform.position = new Vector3(-5.6f, transform.position.y, 0);
        }
        else if (transform.position.x >= 4.3f)
        {
            transform.position = new Vector3(4.3f, transform.position.y, 0);
        }
        



    }

    public void ShipDamage()
    {
        _lives -= 1;

        if (_lives < 1)
        {
            _spawnManager.OnShipDestroyed();
            _spawnManager.DestroyAllClones();
            Destroy(this.gameObject);
        }
    }

    public void AddScore(int points)
    {
        _score += points;
        _uiManager.UpdateScore(_score);
    }

    public void DetractLives(int points)
    {
        _livesUi -= points;
        _uiManager.UpdateLives(_livesUi);
    }

}
