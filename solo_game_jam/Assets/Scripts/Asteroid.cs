﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour
{

    [SerializeField]
    private float _speed = 4.0f;
    private UIManager _uiManager;

    private Spaceship _spaceship;

    void Start()
    {
        _spaceship = GameObject.Find("Spaceship").GetComponent<Spaceship>();
        //Assign spawn position for asteroid
        transform.position = new Vector3(Random.Range(-4.58f, 4.58f), -4.9f, 0);
    }


    void Update()
    {
        transform.Translate(Vector3.down * _speed * Time.deltaTime);

        if (transform.position.y < -23.7f)
        {
            _spaceship.AddScore(10);
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        //Checks if the asteroid hits the spaceship
        if (other.tag == "Spaceship")
        {
            Spaceship spaceship = other.transform.GetComponent<Spaceship>();

            if (spaceship != null)
            {
                _spaceship.DetractLives(1);
                spaceship.ShipDamage();
                Destroy(this.gameObject);
            }
        }
    }
}
