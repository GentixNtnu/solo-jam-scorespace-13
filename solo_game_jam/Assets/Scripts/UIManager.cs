﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    private Text _score;
    [SerializeField]
    private Text _lives;


    void Start()
    {
        _score.text = "Score: " + 0;
        _lives.text = "Lives: " + 3;
    }

    public void UpdateScore(int playerScore)
    {
        _score.text = "Score: " + playerScore.ToString();
    }

    public void UpdateLives(int subtractLife)
    {
        _lives.text = "Lives: " + subtractLife.ToString();
    }
}
