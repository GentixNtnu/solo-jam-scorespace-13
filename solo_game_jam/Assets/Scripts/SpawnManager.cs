﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{

    [SerializeField]
    private GameObject _asteroidPrefab;
    [SerializeField]
    private GameObject _asteroidContainer;
    [SerializeField]
    private float _spawnTimer = 2.0f;
    private bool _isSpaceshipDestroyed = false;


    void Start()
    {
        StartCoroutine(SpawnAsteroidRoutine());
    }


    IEnumerator SpawnAsteroidRoutine()
    {
        while (_isSpaceshipDestroyed == false)
        {
            Vector3 spawnPoint = new Vector3(Random.Range(-4.58f, 3.60f), -4.9f, 0);
            GameObject spawnAsteroid = Instantiate(_asteroidPrefab, spawnPoint, Quaternion.identity);

            spawnAsteroid.transform.parent = _asteroidContainer.transform;

            yield return new WaitForSeconds(_spawnTimer);
        }
    }

    public void OnShipDestroyed()
    {
        _isSpaceshipDestroyed = true;
    }

    public void DestroyAllClones()
    {
        Destroy(this.gameObject);
    }
}
